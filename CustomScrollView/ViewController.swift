//
//  ViewController.swift
//  CustomScrollView
//
//  Created by Александр Евсеев on 15.09.2021.
//

import UIKit

class ViewController: UIViewController {
    
    private let scrollView = CustomScrollView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureScroll()
    }
    
    private func configureScroll() {
        scrollView.backgroundColor = #colorLiteral(red: 0.4745098054, green: 0.8392156959, blue: 0.9764705896, alpha: 1)
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(scrollView)
        
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            scrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            scrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            scrollView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
        ])
    }
}
