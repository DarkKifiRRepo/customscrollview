//
//  ScrollView.swift
//  CustomScrollView
//
//  Created by Александр Евсеев on 15.09.2021.
//

import UIKit

class CustomScrollView: UIScrollView {
    
    override var contentOffset: CGPoint {
        didSet {
            print("offset did change \(contentOffset)")
        }
    }
    
    override var bounds: CGRect {
        didSet {
            print("bounds did change \(bounds)")
        }
    }
    
    override var frame: CGRect {
        didSet {
            print("frame did change \(frame)")
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupViews() {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(stackView)
        
        let view1 = UIView()
        view1.backgroundColor = #colorLiteral(red: 0.5725490451, green: 0, blue: 0.2313725501, alpha: 1)
        stackView.addArrangedSubview(view1)
        
        let view2 = UIView()
        view2.backgroundColor = #colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1)
        stackView.addArrangedSubview(view2)
        
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: topAnchor),
            stackView.leadingAnchor.constraint(equalTo: leadingAnchor),
            stackView.trailingAnchor.constraint(equalTo: trailingAnchor),
            stackView.bottomAnchor.constraint(equalTo: bottomAnchor),
            
            view1.widthAnchor.constraint(equalTo: widthAnchor),
            view1.heightAnchor.constraint(equalToConstant: 400),
            
            view2.widthAnchor.constraint(equalTo: widthAnchor),
            view2.heightAnchor.constraint(equalToConstant: 400),
        ])
    }
}
